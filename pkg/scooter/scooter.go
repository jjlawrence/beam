package scooter

import (
	"context"
	"fmt"

	"bitbucket.org/jjlawrence/beam/pkg/db"
	"bitbucket.org/jjlawrence/beam/pkg/geo"
	"github.com/georgysavva/scany/pgxscan"
	"github.com/google/uuid"
)

// Scooter represents a scooter in space
type Scooter struct {
	UID      uuid.UUID `json:"uid"`
	Location geo.Point `json:"location"`
	// additional meta data, i.e.
	// last service, last use
	// distance
}

// ScooterDistance wraps a Scooter with an extra field
// that is only generated at query execution
type ScooterDistance struct {
	Scooter
	// distance in meters from the origin
	Distance float64 `json:"distance"`
}

// String gives a textual representation of Scooter
func (s *Scooter) String() string {
	return fmt.Sprintf("%s %s", s.UID, s.Location.String())
}

// CreateTable attempts to create the scooters table
func CreateTable(ctx context.Context, db db.DB) error {
	query := `
		CREATE TABLE scooters (
			uid uuid PRIMARY KEY,
			location GEOGRAPHY(POINT)
		)`

	_, err := db.Exec(ctx, query)
	if err != nil {
		return err
	}

	return nil
}

// New takes a long/lat, creates a new uuid and returns
// if it was unable to
func New(long, lat float64) (Scooter, error) {
	uid, err := uuid.NewRandom()
	if err != nil {
		return Scooter{}, err
	}

	location := geo.Point{
		Long: long,
		Lat:  lat,
	}

	scooter := Scooter{
		UID:      uid,
		Location: location,
	}

	return scooter, nil
}

// Create wraps New, but then attempts to insert in to the database
// NOTE: not actually being used in current demo
func Create(ctx context.Context, db db.DB, long, lat float64) (Scooter, error) {
	s, err := New(long, lat)
	if err != nil {
		return Scooter{}, err
	}
	query := `INSERT INTO scooters (uid, location) VALUES ($1, $2)`

	_, err = db.Exec(ctx, query, s.UID, s.Location)
	if err != nil {
		return Scooter{}, err
	}

	return s, nil
}

// FindFromOrigin locates `quantity` scooters within `distance` of `origin`
func FindFromOrigin(ctx context.Context, db db.DB, origin geo.Point, distance, quantity int) ([]ScooterDistance, error) {
	results := []ScooterDistance{}

	query := fmt.Sprintf(`
		SELECT uid, location, ST_Distance(location, $1) AS distance FROM scooters
		WHERE ST_DWithin(location, $1, $2)
		ORDER BY 3
		LIMIT %d
		`,
		quantity,
	)

	err := pgxscan.Select(ctx, db, &results, query, origin, distance)

	return results, err
}
