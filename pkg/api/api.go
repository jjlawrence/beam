package api

import (
	"context"
	"encoding/json"
	"net/http"

	"bitbucket.org/jjlawrence/beam/pkg/db"
	"bitbucket.org/jjlawrence/beam/pkg/geo"
	"bitbucket.org/jjlawrence/beam/pkg/scooter"
)

// API represents an HTTP server with routes for the UI
// to interact with
type API struct {
	db  db.DB
	mux *http.ServeMux
}

// Run initalises an API and its routes before starting it
// returns any errors encounters. NOTE: blocks
func Run(ctx context.Context, db db.DB) error {
	api := API{
		db: db,
	}

	if err := api.setupRoutes(); err != nil {
		return err
	}

	server := http.Server{
		Addr:    ":80",
		Handler: api.mux,
	}

	errCh := make(chan error)
	go func() {
		errCh <- server.ListenAndServe()
	}()

	// wait until context is done or server returns an error
	select {
	case <-ctx.Done():
		return server.Close()
	case err := <-errCh:
		return err
	}
}

// setupRoutes creates and initialises a http.ServeMux
// populaiting it with the API's routes
func (api *API) setupRoutes() error {
	api.mux = http.NewServeMux()

	api.mux.HandleFunc("/api/find", api.handleFind())

	return nil
}

// handleFind returns a function that is able to find scooters
// within requested parameters. We use a closure here as we have
// types that are not being used elsewhere, so we can contain all
// logic in one place
func (api *API) handleFind() http.HandlerFunc {
	type request struct {
		Location geo.Point `json:"location"`
		Distance int       `json:"distance"`
		Quantity int       `json:"quantity"`
	}

	type response struct {
		Results []scooter.ScooterDistance `json:"results"`
	}

	return func(resp http.ResponseWriter, req *http.Request) {
		if req.Method != "POST" {
			http.Error(resp, "Method Not Allowed", http.StatusMethodNotAllowed)
			return
		}

		// bind request params in to a struct
		var params request
		err := json.NewDecoder(req.Body).Decode(&params)
		if err != nil {
			http.Error(resp, err.Error(), http.StatusBadRequest)
			return
		}
		defer req.Body.Close()

		// find results using the scooter package
		results, err := scooter.FindFromOrigin(
			req.Context(),
			api.db,
			params.Location,
			params.Distance,
			params.Quantity,
		)

		// create response
		payload := response{
			Results: results,
		}
		if err != nil {
			http.Error(resp, err.Error(), http.StatusBadRequest)
			return
		}

		// encode and dispatch response, TODO: use sync.Pool
		// with a bytes.Buffer would be more efficient for reading/writing
		resp.Header().Set("Content-Type", "application/json")
		resp.WriteHeader(http.StatusOK)
		json.NewEncoder(resp).Encode(payload)
	}
}
