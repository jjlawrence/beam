package geo

import (
	"bytes"
	"database/sql/driver"
	"encoding/binary"
	"encoding/hex"
	"fmt"
)

// Point represents a position in space
type Point struct {
	Long float64 `json:"long"`
	Lat  float64 `json:"lat"`
}

// String transforms long, lat in to an SRID 4326 (postgis geography) format
func (p *Point) String() string {
	return fmt.Sprintf("SRID=4326;POINT(%v %v)", p.Long, p.Lat)
}

// Scan transforms a postgres postgis geography type in to a Point
func (p *Point) Scan(val interface{}) error {
	b, err := hex.DecodeString(val.(string))
	if err != nil {
		return err
	}
	r := bytes.NewReader(b)
	var wkbByteOrder uint8
	if err := binary.Read(r, binary.LittleEndian, &wkbByteOrder); err != nil {
		return err
	}

	var byteOrder binary.ByteOrder
	switch wkbByteOrder {
	case 0:
		byteOrder = binary.BigEndian
	case 1:
		byteOrder = binary.LittleEndian
	default:
		return fmt.Errorf("Invalid byte order %d", wkbByteOrder)
	}

	var wkbGeometryType uint64
	if err := binary.Read(r, byteOrder, &wkbGeometryType); err != nil {
		return err
	}

	if err := binary.Read(r, byteOrder, p); err != nil {
		return err
	}

	return nil
}

// Value transforms a Point in to a postgres postgis readable format
func (p Point) Value() (driver.Value, error) {
	return p.String(), nil
}
