package bootstrap

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"log"

	"bitbucket.org/jjlawrence/beam/pkg/db"
	"bitbucket.org/jjlawrence/beam/pkg/scooter"
	"github.com/jackc/pgx/v4"
	"github.com/paulmach/orb"
	"github.com/paulmach/orb/geojson"
)

// FromFile attempts to load the data from provided file, for this task it currently
// expects a particular file TODO: it shouldn't expect a particular file
func GeoJSONFromFile(ctx context.Context, db db.DB, file string, pointCount int) error {
	featureCol, err := loadGeoJSON(file)
	if err != nil {
		return err
	}

	for _, f := range featureCol.Features {
		// for the task we are limiting outselves to one region
		// limit us to kml_5 / "CENTRAL REGION"
		if name, ok := f.Properties["Name"]; !ok || name != "kml_5" {
			continue
		}

		// generate random points that we can attach to scooters
		randPoints, err := generatePointsInFeature(ctx, db, f, pointCount)
		if err != nil {
			return err
		}

		// batch import the scooters as it is far more efficient
		batch := pgx.Batch{}

		for _, p := range randPoints {
			s, err := scooter.New(p.Lon(), p.Lat())
			if err != nil {
				return err
			}
			batch.Queue("INSERT INTO scooters (uid, location) VALUES ($1, $2)", s.UID, s.Location)
		}

		br := db.SendBatch(ctx, &batch)
		defer br.Close()

		if _, err := br.Exec(); err != nil {
			return err
		}

		log.Printf("Created %d scooters", len(randPoints))
	}

	return nil
}

// generatePointsInFeature attempts to create count points within the provided geojson feature, it uses PostGIS'
// ST_GeneratePoints to achevie this. TODO: dont use a hardcoded seed
func generatePointsInFeature(ctx context.Context, db db.DB, feat *geojson.Feature, count int) ([]orb.Point, error) {

	query := fmt.Sprintf(
		`SELECT ST_AsGeoJSON(ST_GeneratePoints(ST_GeomFromGeoJSON($1::jsonb->'geometry'), %d, 1996))`,
		count,
	)

	b, err := feat.MarshalJSON()
	if err != nil {
		return nil, err
	}

	var data []byte

	err = db.QueryRow(ctx, query, b).Scan(&data)
	if err != nil {
		return nil, err
	}

	geom, err := geojson.UnmarshalGeometry(data)
	if err != nil {
		return nil, err
	}

	points, ok := geom.Coordinates.(orb.MultiPoint)
	if !ok {
		return nil, errors.New("invalid geom type")
	}

	return points, nil
}

// load a geojson file from disk into a FeatureCollection
func loadGeoJSON(file string) (*geojson.FeatureCollection, error) {
	b, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}

	feature, err := geojson.UnmarshalFeatureCollection(b)
	if err != nil {
		return nil, err
	}

	return feature, nil
}
