# BEAM!
This my attempt at the provided [coding
challenge](https://docs.google.com/document/u/1/d/e/2PACX-1vSYFyD4CW9_M1YoMIBNCtPbcXf4jUMbVnK-RtsGf-H41VYYEiCnktbmiR67vmniDJT5AdR_Fd2mZbNO/pub).

## TLDR
Wall of text is too much. Give me the commands:

Run `docker-compose up`, wait for `Starting api ....` open [http://localhost:5000](http://localhost:5000)

## Repo Structure
Some information about navigating the repo:

- `docker/` - Dockerfile's required by docker-compose
- `assets/` - Contains the Singapore boundary data used to bootstrap the database
- `pkg/`    - The Go package that bootstraps and then serves data to the
  frontend
- `ui/`     - The Svelte frontend used to display and interact with the database
- `.env`    - This contains configurable options, the most important being
  `BOOTSTRAP_SCOOTER_COUNT` which dictates the number of scooters/locations to add
  to the database
- `docker-compose.yml` - This describes the environment required to deploy the
  project. It contains three services: `postgres`, `api`, `ui`
- `main.go` - Entry point for the Go program

## Architecture
This project relies heavily on the incredible [PostGIS](https://postgis.net/)
postgres extension which provides lots of functionality around geographical
points (as well as other stuff).

The Go service can be broken down in to the following packages:

- `api/` - Small HTTP API for serving data to the frontend
- `bootstrap/` - Functionality for importing data into the database
- `db/` - An interface around our database connection which allows us to create
  the code in a testable way.
- `geo/` - Small library to allow us to support PostGIS' `GEOGRAPHY(POINT)` type
- `scooter/`

Our `scooters` table is very simple:

```
	CREATE TABLE scotters (
		uid uuid PRIMARY KEY,
		location GEOGRAPHY(POINT)
		-- space to add lots of meta data needed in reality
	)
```

Next we use the publicly available [boundary
data](https://data.gov.sg/dataset/), for this task I opted for [Master Plan
2019](https://data.gov.sg/dataset/master-plan-2019-region-boundary-no-sea), but
in practice smaller polgyons would be better, i.e.
[Constituencies](https://data.gov.sg/dataset/electoral-boundary_2020) (Explained
in scaling document).

For this I only used one polygon (called `kml_5`, or `CENTRAL REGION`) to populate random
points, to achieve this I once again used postgis:

```
	SELECT ST_AsGeoJSON(ST_GeneratePoints(ST_GeomFromGeoJSON($1::jsonb->'geometry'), %d, 1996))
```

Where `$1` is the Geometry field from our geojson master plan Feature, `%d` is
the count of points we want to generate, and 1996 is a seed value.

My initial plan was to do this in Go, but I opted to relying on postgis in the
end, to alleviate more complexity from the code base.

Once these points have been batch imported as `Scooter`s the API is started with
a very simply `POST` endpoint at `/api/find`, which takes an origin point, a
distance and a quantity, and then searches for nearby scooters with the
following query:

```
	SELECT uid, location, ST_Distance(location, $1) AS distance 
	FROM scooters
	WHERE ST_DWithin(location, $1, $2)
	ORDER BY 3
	LIMIT %d
```

Where `$1` is the "origin", `$2` is the distance in meters and `%d` is the
quantity we would like. On datasets of a few hundred thousand, this is very
quick, but it of course has room for a lot of improvement, please see scaling
document for more information).

The UI is written in [svelte](https://svelte.dev/) which is as fast and
featureful as React, but allows for much quicker (in my opinion) prototyping as
there is much less boilerplate and complexity (mainly from redux). For styling I
used a small CSS toolkit called [tachyons](https://tachyons.io/).

## Running
Before running, please edit `.env`'s `BOOTSTRAP_SCOOTER_COUNT` to a number of
your choice, 500,000 ran on my laptop (Thinkpad T480/32GB RAM) with no issues:

```
api_1       | 2021/04/06 09:53:44 Bootstrapping data ....
api_1       | 2021/04/06 09:53:48 Created 500002 scooters
```

*NOTE*
Data will not be accessible until you see the following:
```
api_1       | 2021/04/07 00:37:59 Starting api ....
```

This project assumes that `docker` and `docker-compose` are available. To start
simply run: `docker-compose up` (CTRL+C to shutdown)

To remove: `docker-compose down --volumes`

Once started it will be available at:
[http://localhost:5000](http://localhost:5000) I also added an
[adminer](https://www.adminer.org) service to easily be able to browse the
database. Credentials are `beam`/`beam` or whatever was configured in `.env` and
is available at [http://localhost:8080](http://localhost:8080).

When testing, I do not recommend requesting too many scooters, 5000 seemed to be
OK for me. I also added some debouncing to the request to try and smooth fast
clicking.
