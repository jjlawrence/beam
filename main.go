package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"strconv"

	"bitbucket.org/jjlawrence/beam/pkg/api"
	"bitbucket.org/jjlawrence/beam/pkg/bootstrap"
	"bitbucket.org/jjlawrence/beam/pkg/db"
	"bitbucket.org/jjlawrence/beam/pkg/scooter"
	"github.com/jackc/pgx/v4/pgxpool"
)

// main wraps run and provides us with a nice and easy
// way to propogate errors
func main() {
	if err := run(); err != nil {
		fmt.Fprintf(os.Stderr, "Error: %s\n", err)
		os.Exit(1)
	}
}

func run() error {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	log.Println("Connecting to database ....")
	pool, err := pgxpool.Connect(ctx, os.Getenv("POSTGRES_URI"))
	if err != nil {
		return err
	}
	defer pool.Close()

	log.Println("Ensuring Tables ....")
	if err := createTables(ctx, pool); err != nil {
		return err
	}

	log.Println("Bootstrapping data ....")

	// check to see how many scooters we have in the database
	var count int
	err = pool.QueryRow(ctx, "SELECT COUNT(*) FROM scooters").Scan(&count)
	if err != nil {
		return err
	}

	expected, err := strconv.Atoi(os.Getenv("BOOTSTRAP_SCOOTER_COUNT"))
	if err != nil {
		return err
	}

	// if its less than expected, bootstrap the difference
	if count < expected {
		expected -= count
		err = bootstrap.GeoJSONFromFile(ctx, pool, "assets/master-plan-2019-region-boundary-no-sea-geojson.geojson", expected)
		if err != nil {
			return err
		}
	}

	log.Println("Starting api ....")
	if err := api.Run(ctx, pool); err != nil {
		return err
	}

	return nil
}

// ensure that the postgres tables are created
func createTables(ctx context.Context, db db.DB) error {
	if err := scooter.CreateTable(ctx, db); err != nil {
		if err.Error() != `ERROR: relation "scooters" already exists (SQLSTATE 42P07)` {
			return err
		}
	}
	return nil
}
