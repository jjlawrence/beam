FROM node 

WORKDIR /build

COPY package.json ./
COPY package-lock.json ./
COPY rollup.config.js ./
COPY public ./public
COPY src ./src

RUN npm install

CMD npm run dev
