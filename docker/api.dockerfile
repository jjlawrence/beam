FROM golang:alpine

WORKDIR /build

# download dependencies
COPY go.mod go.sum ./
RUN go mod download

# copy in the source code
COPY . .

# hot reload
RUN go install github.com/githubnemo/CompileDaemon


ENTRYPOINT CompileDaemon \
	-log-prefix=false \
	-build="go build -o /bin/api main.go" \
	-command="/bin/api"
