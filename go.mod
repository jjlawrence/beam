module bitbucket.org/jjlawrence/beam

go 1.16

require (
	github.com/Taetse/spatial v0.0.1-2020
	github.com/georgysavva/scany v0.2.8
	github.com/google/uuid v1.0.0
	github.com/jackc/pgconn v1.8.1
	github.com/jackc/pgx v3.6.2+incompatible
	github.com/jackc/pgx/v4 v4.11.0
	github.com/paulmach/orb v0.2.1
)
